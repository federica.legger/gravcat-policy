# -*- coding: utf-8 -*-
# Copyright James Alexander Clark 2021
#
# This file is part of gravcat_policy.
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GWDataFind is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Rucio policy plugin for the International Gravitational Wave Network.
"""

__author__ = 'James Alexander Clark <james.clark@ligo.org>'
__version__ = '1.0.0'

# NOTE: doing this import in here
# (https://rucio.readthedocs.io/en/latest/policy_packages.html) results in a
# circular dependency which breaks pretty much everything.  Instead, use the
# configuraiton value lfn2pfn_module: "gravcat_policy.lfn2pfn"

#   from gravcat_policy.lfn2pfn import igwn_frames
#   from rucio.rse.protocols.protocol import RSEDeterministicTranslation
#   RSEDeterministicTranslation.register(igwn_frames, 'igwn_frames')

SUPPORTED_VERSION = [ "1.29", "1.30", "1.31", "32" ]

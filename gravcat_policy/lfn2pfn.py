# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2021
# - Federica Legger, <federica.legger@to.infn.it>, 2023

"""LFN-to-PFN algorithms for IGWN.
"""
import re
from rucio.rse.protocols.protocol import RSEDeterministicTranslation

# See: https://dcc.ligo.org/LIGO-T010150

LDAS_PATH_RE = re.compile(r'([A-Z]+)-([A-Za-z0-9_]+)-([0-9]{5})[0-9]+-[0-9]+.[A-Za-z]+')

_TESTS_RE = re.compile(r'^automatix')

# Virgo frames
_GWF_RE = re.compile(r'([A-Z]+)-([A-Za-z0-9]+)-([0-9]{5,5})(.*)')

# noinspection PyUnusedLocal
def igwn_frames(scope: str, lfn: str, rse: str = None, rse_attrs: dict = None,
                proto_attrs: dict = None) -> str:
    """
    This function maps the scope:LFN to the PFN for bulk data products which
    obey the naming convention https://dcc.ligo.org/LIGO-T010150.

    This function maps an LDAS-compliant LFN to a PFN determined solely from the LFN. E.g.:
    `H-H1_R-1322700032-64.gwf -> H/H1_R/H-H1_R-13227/H-H1_R-1322700032-64.gwf`,
    where the first 5 GPS digits are used for the parent directory.

    Automatix-based functionality testing files are simply placed in a single parent directory
    determined by the scope.  E.g.,:
    `scope:automatix.123 -> scope/automatix.123`

    Parameters
    ----------
    scope : `str`
        Rucio namespace scope for file.
    lfn : `str`
        Logical filename.
    rse : `str`
        Name of RSE file is at.  Unused, retained for standard function signature.
    rse_attrs : `dict`
        Dictionary of RSE attributes.  Unused, retained for standard function signature.
    proto_attrs: `dict`
        Dictionary of RSE protocol attributes.  Unused, retained for standard function signature.

    """
    # pylint: disable=unused-argument

    # TODO: parse scope for obs_run / add support for e.g. LIGO.frames.O3 -> <prefix>/O3/path
    obs_run = scope

    # Automatix test data
    if _TESTS_RE.search(lfn):
        pfn = f"{scope}/{lfn}"

    # LDAS scheme
    else:
        try:
            obs, content, gps_prefix = LDAS_PATH_RE.match(lfn).groups()
        except AttributeError as match_failed:
            raise AttributeError(f"Invalid LDAS filename: {lfn}.  See: "
                                 "https://dcc.ligo.org/LIGO-T010150.") from match_failed

        dataset_dir = f"{obs}-{content}-{gps_prefix}"

        pfn = f"{obs_run}/{content}/{dataset_dir}/{lfn}"

    return pfn

RSEDeterministicTranslation.register(igwn_frames, 'igwn_frames')

def virgo_stashcache(scope: str, lfn: str, rse: str = None, rse_attrs: dict = None,
                proto_attrs: dict = None) -> str:
    """
    This function maps the Virgo GWF files according to the following schema:

    O3:V-V1O3Repro1A-1252540000-5000.gwf ->
    O3/V1O3Repro1A/V-V1O3Repro1A-12525/V-V1O3Repro1A-1252540000-5000.gwf

    Such schema corresponds to the one used on stashcache/xcache
    endpoints for CVMFS data distribution.


    Parameters
    ----------
    scope : `str`
        Rucio namespace scope for file.
    lfn : `str`
        Logical filename.
    rse : `str`
        Name of RSE file is at.  Unused, retained for standard function signature.
    rse_attrs : `dict`
        Dictionary of RSE attributes.  Unused, retained for standard function signature.
    proto_attrs: `dict`
        Dictionary of RSE protocol attributes.  Unused, retained for standard function signature.

    """
    # pylint: disable=unused-argument

    # Automatix test data
    if _TESTS_RE.search(lfn):
        pfn = f"{scope}/{lfn}"

    match = _GWF_RE.match(lfn)

    if not match:
        raise ValueError("Invalid Virgo filename. Should follow the schema O3:V-V1O3Repro1A-1252540000-5000.gwf -> O3/V1O3Repro1A/V-V1O3Repro1A-12525/V-V1O3Repro1A-1252540000-5000.gwf")

    detector, dataset, gps_prefix, _ = match.groups()
    dir_hash = "%s-%s-%s" % (detector, dataset, gps_prefix)

    if scope == 'AdVirgo':
        dataset = 'HrecOnline'

    pfn = '%s/%s/%s/%s' % (scope, dataset, dir_hash, lfn)

    return pfn

RSEDeterministicTranslation.register(virgo_stashcache, 'virgo_stashcache')

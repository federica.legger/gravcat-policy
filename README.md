# gravcat-policy

Policy packages are separate Python packages that can be used to add
experiment-specific customisations to Rucio. They typically customise Rucio’s
handling of permissions and schema as well as optionally adding their own
algorithms for lfn to pfn conversion and surl construction.

See [rucio docs](https://rucio.readthedocs.io/en/old-doc/policy_packages.html).

This package includes the Rucio plugin algorithms for LFN to PFN conversion for
rucio in the International Gravitational Wave Network.
